var nameUser = prompt('Введите ваше имя: ');


//Generate number
var n1 = rnd(10, 1500);
var n2 = rnd(10, 1500);
var n3 = rnd(10, 1500);
var n4 = rnd(10, 1500);
var n5 = rnd(10, 1500);
var n6 = rnd(10, 1500);

var n12 = rnd(10, 1500);
var n22 = rnd(10, 1500);
var n32 = rnd(10, 1500);
var n42 = rnd(10, 1500);
var n52 = rnd(10, 1500);
var n62 = rnd(10, 1500);

//Inputs
var in1 = document.querySelector('#in1');
var in2 = document.querySelector('#in2');
var in3 = document.querySelector('#in3');
var in4 = document.querySelector('#in4');
var in5 = document.querySelector('#in5');
var in6 = document.querySelector('#in6');

var in12 = document.querySelector('#in12');
var in22 = document.querySelector('#in22');
var in32 = document.querySelector('#in32');
var in42 = document.querySelector('#in42');
var in52 = document.querySelector('#in52');
var in62 = document.querySelector('#in62');

//Original number
var tn1 = document.querySelector('#tn1');
var tn2 = document.querySelector('#tn2');
var tn3 = document.querySelector('#tn3');
var tn4 = document.querySelector('#tn4');
var tn5 = document.querySelector('#tn5');
var tn6 = document.querySelector('#tn6');

var tn12 = document.querySelector('#tn12');
var tn22 = document.querySelector('#tn22');
var tn32 = document.querySelector('#tn32');
var tn42 = document.querySelector('#tn42');
var tn52 = document.querySelector('#tn52');
var tn62 = document.querySelector('#tn62');

//Result
var r1 = document.querySelector('#r1');
var r2 = document.querySelector('#r2');
var r3 = document.querySelector('#r3');
var r4 = document.querySelector('#r4');
var r5 = document.querySelector('#r5');
var r6 = document.querySelector('#r6');

var r12 = document.querySelector('#r12');
var r22 = document.querySelector('#r22');
var r32 = document.querySelector('#r32');
var r42 = document.querySelector('#r42');
var r52 = document.querySelector('#r52');
var r62 = document.querySelector('#r62');

tn1.innerHTML = '<b>' + n1.toString(2) + '</b>';
tn12.innerHTML = '<b>' + n12.toString(2) + '</b>';

tn2.innerHTML = '<b>' + n2.toString(10) + '</b>';
tn22.innerHTML = '<b>' + n22.toString(10) + '</b>';

tn3.innerHTML = '<b>' + n3.toString(8) + '</b>';
tn32.innerHTML = '<b>' + n32.toString(8) + '</b>';

tn4.innerHTML = '<b>' + n4.toString(10) + '</b>';
tn42.innerHTML = '<b>' + n42.toString(10) + '</b>';

tn5.innerHTML = '<b>' + n5.toString(16) + '</b>';
tn52.innerHTML = '<b>' + n52.toString(16) + '</b>';

tn6.innerHTML = '<b>' + n6.toString(10) + '</b>';
tn62.innerHTML = '<b>' + n62.toString(10) + '</b>';


var attempt = 0; // количество попыток
var right = 0;
var wrong = 0;

document.querySelector('#check').onclick = function() {
	if(n1 == in1.value) {
		r1.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r1.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n12 == in12.value) {
		r12.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r12.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}	
	
	if(n2.toString(2) == in2.value) {
		r2.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r2.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n22.toString(2) == in22.value) {
		r22.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r22.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}	

	if(n3.toString(10) == in3.value) {
		r3.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r3.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n32.toString(10) == in32.value) {
		r32.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r32.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}	

	if(n4.toString(8) == in4.value) {
		r4.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r4.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n42.toString(8) == in42.value) {
		r42.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r42.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}	
	
	if(n5.toString(10) == in5.value) {
		r5.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r5.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n52.toString(10) == in52.value) {
		r52.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r52.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	
	if(n6.toString(16) == in6.value) {
		r6.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r6.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	if(n62.toString(16) == in62.value) {
		r62.innerHTML = '<b>Верно</b>';
		right += 1;
	} else {
		r62.innerHTML = '<b>Неверно</b>';
		wrong += 1;
	}
	
	var msg = 'name=' + nameUser + '&right=' + right + '&wrong=' + wrong;
 	$.ajax({
 	  	type: 'POST',
 	  	url: '/register',
 	  	data: msg,
 	  	success: function (data) {
 	  		alert('Ты записан в системе как:  ' + data)
 	  	},
 	  	error: function (xhr, str) {
 	  		alert('Возникла ошибка: ' + xhr.responseCode);
 	  	}
 	  });
}
