/*
rnd(min[number], max[number]) - функция генерации целого случайного числа
	min[number] - минимальное значение
	max[number] - максимальное значение
*/


function rnd(min, max) {
    var rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}


  $(document).ready(function () {
	  /*
	  Данный код отвечает за открытие модального окна
	  */
	  $('.modal').modal();
  });