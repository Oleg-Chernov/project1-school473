//Generate number
var n1 = rnd(10, 1000);
var n2 = rnd(10, 1000);
var n3 = rnd(10, 1000);
var n4 = rnd(10, 1000);
var n5 = rnd(10, 1000);
var n6 = rnd(10, 1000);

//Inputs
var in1 = document.querySelector('#in1');
var in2 = document.querySelector('#in2');
var in3 = document.querySelector('#in3');
var in4 = document.querySelector('#in4');
var in5 = document.querySelector('#in5');
var in6 = document.querySelector('#in6');

//Original number
var tn1 = document.querySelector('#tn1');
var tn2 = document.querySelector('#tn2');
var tn3 = document.querySelector('#tn3');
var tn4 = document.querySelector('#tn4');
var tn5 = document.querySelector('#tn5');
var tn6 = document.querySelector('#tn6');

//Result
var r1 = document.querySelector('#r1');
var r2 = document.querySelector('#r2');
var r3 = document.querySelector('#r3');
var r4 = document.querySelector('#r4');
var r5 = document.querySelector('#r5');
var r6 = document.querySelector('#r6');

tn1.innerHTML = '<b>' + n1.toString(2) + '</b>';
tn2.innerHTML = '<b>' + n2.toString(2) + '</b>';
tn3.innerHTML = '<b>' + n3.toString(2) + '</b>';
tn4.innerHTML = '<b>' + n4.toString(2) + '</b>';
tn5.innerHTML = '<b>' + n5.toString(2) + '</b>';
tn6.innerHTML = '<b>' + n6.toString(2) + '</b>';

var attempt = 0; // количество попыток

document.querySelector('#check').onclick = function() {
	if(n1 == in1.value) {
		r1.innerHTML = '<b>Верно</b>';
	} else {
		r1.innerHTML = '<b>Неверно</b>';
	}
	
	if(n2 == in2.value) {
		r2.innerHTML = '<b>Верно</b>';
	} else {
		r2.innerHTML = '<b>Неверно</b>';
	}
	
	if(n3 == in3.value) {
		r3.innerHTML = '<b>Верно</b>';
	} else {
		r3.innerHTML = '<b>Неверно</b>';
	}
	
	if(n4 == in4.value) {
		r4.innerHTML = '<b>Верно</b>';
	} else {
		r4.innerHTML = '<b>Неверно</b>';
	}
	
	if(n5 == in5.value) {
		r5.innerHTML = '<b>Верно</b>';
	} else {
		r5.innerHTML = '<b>Неверно</b>';
	}
	
	if(n6 == in6.value) {
		r6.innerHTML = '<b>Верно</b>';
	} else {
		r6.innerHTML = '<b>Неверно</b>';
	}
	
	attempt += 1;
	Materialize.toast('Попытка ' + attempt, 1000, 'rounded');
}