var express = require("express");
var bodyParser = require("body-parser");
var colors = require("colors");
var fs = require("fs");

var app = express();
 
var statusTest = 1;
// создаем парсер для данных application/x-www-form-urlencoded
var urlencodedParser = bodyParser.urlencoded({extended: false});
 
app.use(express.static(__dirname + "/public"));
 
app.post("/register", urlencodedParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    console.log(request.body);
	var nameUser = request.body.name;
	var right = request.body.right;
	var wrong = request.body.wrong;
	
	fs.appendFileSync('file.txt', `\n<li class="collection-item"><b>Имя</b>: ${nameUser} <b>Правильных ответов</b>: ${right} <b>Неправильных ответов</b>: ${wrong}</li>`);
  response.send(`${request.body.name}`);
});

app.post('/check', urlencodedParser, function(request, response) {
	if(!request.body) return response.sendStatus(400);
	var content = fs.readFile("file.txt", "utf8", function(error,data){ 
		response.send(data)
	});
	
});

app.post('/stopTesting', urlencodedParser, function(request, response) {
	if(!request.body) return response.sendStatus(400);
	if(request.body.user == 'admin') {
		fs.writeFile("file.txt", "");
		statusTest = 0;
	}
	if(request.body.user == 'user') {
		response.send(statusTest);
	}
	
});

app.get("/", function(request, response){
     response.send("<h1>Главная страница</h1>");
	 console.log(request.ip)
});
 
app.listen(3000);

console.log('The test is at http://ip:3000'.green.bold);
console.log('Admin panel http://ip:3000/admin.html'.green.bold);
console.log('The project was founded by Oleg Chernov and is under the license of MIT'.dim);
console.log('Offical page at project https://gitlab.com/Oleg-Chernov/project1-school473'.dim)